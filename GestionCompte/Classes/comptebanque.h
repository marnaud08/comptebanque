/*
 * comptebanque.h
 *
 *  Created on: 4 mars 2022
 *      Author: michel
 */

#ifndef COMPTEBANQUE_H_
#define COMPTEBANQUE_H_

class CompteBanque {
	float solde;
public:
//	CompteBanque();
	CompteBanque(float so=20);
	virtual ~CompteBanque();
	void afficherSolde();
	float getSolde();
	void virementCompte(float val);
	float retirer(float val);
};

#endif /* COMPTEBANQUE_H_ */
